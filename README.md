![Screenshot preview of the theme "Resonance" by glenthemes](https://static.tumblr.com/gtjt4bo/bnps6iaju/resonancepv05.png)

**Theme no.:** 32  
**Theme name:** Resonance  
**Theme type:** Free / Tumblr use  
**Description:** This theme is a remake of an [old unreleased design](https://64.media.tumblr.com/d92756ced4c5682868c4fad7021f0d1d/tumblr_nzmzkrWs0O1ubolzro1_1280.png). It consists of a sidebar and information boxes, and features Jirou Kyouka from Boku no Hero Academia.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2018-07-25  
**Revamp date:** 2023-12-31

**Post:** [glenthemes.tumblr.com/post/176263839584](https://glenthemes.tumblr.com/post/176263839584)  
**Preview:** [glenthpvs.tumblr.com/resonance](https://glenthpvs.tumblr.com/resonance)  
**Download:** [pastebin.com/ivPYDzsf](https://pastebin.com/ivPYDzsf)
