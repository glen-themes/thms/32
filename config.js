/*------------------------------------------------------------------------------
                                                        
      Written by @glenthemes
      Please do NOT copy unless I gave you permission.
      2023 ⓒ All Rights Reserved
      
      Credits:
      > play button icon: flaticon.com/free-icon/play_727245
      > pause button icon: flaticon.com/free-icon/pause_9847130
      > minimal soundcloud player: shythemes.tumblr.com/post/114792480648

-------------------------------------------------------------------------------*/

let currentURL = window.location.href;
if(currentURL.slice(-1) == "/"){
  currentURL = currentURL.slice(0,-1);
}
if(currentURL.indexOf("#") > -1){
  currentURL = currentURL.split("#")[0]
}
if(currentURL.indexOf("?") > -1){
  currentURL = currentURL.split("?")[0]
}
let currentPath = window.location.pathname;
if(currentPath.slice(-1) == "/"){
  currentPath = currentPath.slice(0,-1);
}

/*------- MASK SVGS -------*/
// play icon
// flaticon.com/free-icon/play_727245
let xhkot = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 320.001 320.001' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><path d='m295.84 146.049-256-144a16.026 16.026 0 0 0-15.904.128A15.986 15.986 0 0 0 16 16.001v288a15.986 15.986 0 0 0 7.936 13.824A16.144 16.144 0 0 0 32 320.001c2.688 0 5.408-.672 7.84-2.048l256-144c5.024-2.848 8.16-8.16 8.16-13.952s-3.136-11.104-8.16-13.952z' fill='black' data-original='black' class=''></path></g></svg>";

document.documentElement.style.setProperty("--PlayIconMask", `url("${xhkot}")`);

// pause icon
// flaticon.com/free-icon/pause_9847130
let vteumj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 24 24' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><g data-name='Layer 2'><rect width='5' height='20' x='4.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect><rect width='5' height='20' x='14.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect></g></g></svg>";

document.documentElement.style.setProperty("--PauseIconMask",`url("${vteumj}")`);

/*-------------------------------------------------*/

document.addEventListener("DOMContentLoaded", () => {
  let pvmarker = document.querySelectorAll(".tumblr_preview_marker___");
  pvmarker?.forEach(marker => {
      marker.remove();
  })
  
  document.documentElement.setAttribute("current-url",currentPath)
  
  /*------- REMOVE HREF.LI LINK REFERRER -------*/
  const noHrefLi = () => {
    document.querySelectorAll("a[href*='href.li/?']")?.forEach(a => {
      let href = a.href;
      if(href.slice(0,17) == "https://href.li/?"){
        a.href = href.slice(17)
      }
    })
  }
  
  /*------- NPF MISC -------*/
  const npfMisc = () => {
    // stop npf caption text from sticking together
    // this only happens sometimes but I have no idea when it procs
    let npf_post = document.querySelectorAll("[post-type='text'] .post-body > *:not(.npf_group)");
    npf_post?.forEach(npf_post => {
        npf_post.querySelectorAll("b, i, em, strong, u, span").forEach(st => {
            if(st.innerHTML.trim() == ""){
                st.textContent = " "
            }
        })
    })
  }
  
  /*------- CHECK OP -------*/
  const theRealOP = () => {
    document.querySelectorAll(".posts[post-type='text'][username]:not([username=''])[root-url]:not([root-url=''])")?.forEach(post => {
      let actualOP = post.getAttribute("username");
      let actualOP_Post_URL = post.getAttribute("root-url");
      let firstHeader = post.querySelector(".caption > .reblogs:first-of-type:not(.original-entry)[username]:not([username=''])");
      if(firstHeader){
        let firstHeadUser = firstHeader.getAttribute("username");
        if(firstHeadUser !== actualOP){
          let avstr = `https://api.tumblr.com/v2/blog/${actualOP}/avatar/64`;
          let a = firstHeader.querySelector("a.reblog-head");
          let img = firstHeader.querySelector(".reblog-head img");
          let span = firstHeader.querySelector(".reblog-user");
          
          let i_m_g = new Image();
          i_m_g.src = avstr;
          img ? img.src = avstr : null          
          img ? img.alt = img.alt.replaceAll(firstHeadUser,actualOP) : null          
          firstHeader.setAttribute("username",actualOP);
          firstHeader.classList.add("original-entry");
          a ? a.href = actualOP_Post_URL : null
          span ? span.textContent = actualOP : null          
        }
      }
    })
  }
  
  /*------- LEGACY PHOTO(SET) -------*/  
  function legacyIMG(w, h, ld, hd){
    this.width = Number(w);
    this.height = Number(h);
    this.low_res = ld;
    this.high_res = hd;
  }

  const handleLegacyPhotos = () => {
    let old_photoset = document.querySelectorAll(".legacy-photoset[layout]");
    old_photoset?.forEach((sett) => {
      let sett_ly = sett.getAttribute("layout");
      let sett_ar = [];
      let sett_xy = sett_ly.split("");
      
      sett_xy?.forEach(zett => {
        sett_ar.push(zett)
      })

      sett_ar.forEach((xett, i) => {
        let pylnn = document.createElement("div");
        pylnn.classList.add("layout-row");
        pylnn.setAttribute("cols", xett);
        pylnn.style.gridTemplateColumns = `repeat(${xett}, 1fr)`
        sett.append(pylnn);
      });

      sett.querySelectorAll(".layout-row:empty").forEach((lr, i) => {
        let curNum = parseInt(lr.getAttribute("cols"));
        sett.querySelectorAll("img").forEach((boo, i) => {
          i += 1;
          if(i <= curNum){
            lr.append(boo);
          }
        });
      });
      
    }); // end .legacy-photoset forEach
    
    document.querySelectorAll(".legacy-photo, .legacy-photoset")?.forEach(sel => {
      let mbusv = [];
      sel.querySelectorAll("img").forEach((imgz, index) => {
        mbusv.push(new legacyIMG(
          imgz.getAttribute("width"),
          imgz.getAttribute("height"),
          imgz.src,
          imgz.src
        ));
        
        let bipuq = Math.floor(parseInt(index)+1);
        imgz.setAttribute("index",bipuq);
        sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},1)`);
        imgz.addEventListener("click", () => {
          sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},${bipuq})`);
        })
      })
    })
  };

  /*------- AUDIO STUFF -------*/
  // audio volume
  let audVol = getRoot("--Audio-Post-Volume");
  audVol = audVol.indexOf("%") > -1 ? parseFloat(audVol) / 100 : parseFloat(audVol);
  
  /*------- LEGACY AUDIO -------*/
  const legacyAudio = () => {
    document.querySelectorAll(".aud-gen .aud-info + .aud-iframe iframe.tumblr_audio_player")?.forEach((aud_embed) => {
      let audPostID = aud_embed.closest("[post-type='audio'][id]").getAttribute("id").replaceAll("post-","");

      let audioIMGArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-cover img");
      let audioPlay = aud_embed.closest(".aud-gen")?.querySelector(".q-play");
      let audioPause = aud_embed.closest(".aud-gen")?.querySelector(".q-pause");
      let audioTextArea = aud_embed.closest(".aud-iframe")?.previousElementSibling;
      let audioDLArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-dl");

      aud_embed.addEventListener("load", () => {
        let contents = aud_embed.contentDocument;
        
        let audSrc, audTitle, audArtist, audAlbumIMG, audAlbumName;
        
        // IS NULL
        if(contents === null){
          let useSrc = aud_embed.src;
          let trem = useSrc.split("?audio_file=")[1].split("&")[0];
          audSrc = decodeURIComponent(trem)
        }
        
        // IS NOT NULL
        else {
          audSrc = contents.querySelector("[data-stream-url]")?.getAttribute("data-stream-url");
          audTitle = contents.querySelector("[data-track]")?.getAttribute("data-track");
          audArtist = contents.querySelector("[data-artist]")?.getAttribute("data-artist");
          audAlbumIMG = contents.querySelector("[data-album-art]")?.getAttribute("data-album-art");
          audAlbumName = contents.querySelector("[data-album]")?.getAttribute("data-album");
        }//end: is NOT null
        

        // assign image
        // if(audioIMGArea){
        //   audioIMGArea.src = audAlbumIMG;
        // }

        // bind audio url to download button
        audioDLArea.href = audSrc;

        // create an audio element
        let newAud = document.createElement("audio");
        newAud.src = audSrc;
        aud_embed.closest(".aud-gen").append(newAud);
        newAud.volume = audVol;

        audioPlay.addEventListener("click", () => {
          if(newAud.paused){
            newAud.play();
          }
        });

        audioPause.addEventListener("click", () => {
          if(!newAud.paused){
            newAud.pause();
          }
        });

        newAud.addEventListener("play", () => {
          audioPlay.style.display = "none";
          audioPause.style.display = "flex";
        });

        newAud.addEventListener("pause", () => {
          audioPause.style.display = "none";
          audioPlay.style.display = "flex";
        });

        newAud.addEventListener("ended", () => {
          audioPause.style.display = "none";
          audioPlay.style.display = "flex";
        });
      }); //end audio load
    }); // end audio each (legacy)
  }; //end legacyAudio()

  
  /*------- NPF AUDIO -------*/
  const neueAudio = () => {
    document.querySelectorAll("figcaption.audio-caption")?.forEach((npfAudio) => {
      // check if there's anything preceding text/content
      let prev = npfAudio.previousElementSibling;

      // check if there's anything after it
      let next = npfAudio.nextElementSibling;

      if(next){
        if(next.innerHTML.trim() == ""){
          npfAudio.classList.add("no-next");
        }
      } else {
        npfAudio.classList.add("no-next");
      }

      if(prev){
        if(prev.innerHTML.trim() == ""){
          npfAudio.classList.add("no-prev");
        }
      } else {
        npfAudio.classList.add("no-prev");
      }

      if(npfAudio.matches(".no-next")){
        let nextOfAud = npfAudio.nextElementSibling;

        // NotReblog, has no capt: remove .caption
        if(nextOfAud.matches(".original-post")){
          nextOfAud.remove();
        }

        // RebloggedFrom, has no capt, remove .original-entry
        else {
          if(nextOfAud.querySelector(".original-entry")){
            nextOfAud.querySelector(".original-entry").remove();
          }
        }
      }

      npfAudio.classList.remove("no-prev");
      npfAudio.classList.remove("no-next");

      let npfAudioDiv = document.createElement("div");
      npfAudioDiv.classList.add("aud-gen");
      npfAudio.before(npfAudioDiv);

      npfAudio.querySelectorAll(":scope > *").forEach((h) => {
        npfAudioDiv.append(h);
      });

      npfAudio.remove();

      npfAudioDiv.querySelectorAll(".tmblr-audio-meta").forEach((m) => {
        m.classList.remove("tmblr-audio-meta");
      });

      let deets = document.createElement("div");
      deets.classList.add("aud-info");

      let oldDeets = npfAudioDiv.querySelector(".audio-details");
      oldDeets.before(deets);
      oldDeets.querySelectorAll(":scope > *").forEach((o) => {
        deets.append(o);
      });

      oldDeets.remove();

      let xyz = document.createElement("div");
      xyz.classList.add("aud-xyz");
      deets.before(xyz);
      xyz.append(deets);

      let npfAudioTitle = npfAudioDiv.querySelector(".title");
      let npfAudioArtist = npfAudioDiv.querySelector(".artist");
      let npfAudioAlbum = npfAudioDiv.querySelector(".album");

      if(npfAudioTitle){
        let titleText = npfAudioTitle.textContent;
        if(titleText.trim() == ""){
          titleText = "Untitled Track";
        }
        let titleDiv = document.createElement("div");
        titleDiv.classList.add("aud-title");
        titleDiv.textContent = titleText;
        npfAudioTitle.before(titleDiv);
        npfAudioTitle.remove();
      }

      if(npfAudioArtist){
        let artistText = npfAudioArtist.textContent;
        if(artistText.trim() == ""){
          artistText = "Unknown Artist";
        }
        let artistDiv = document.createElement("div");
        artistDiv.classList.add("aud-artist");
        artistDiv.textContent = artistText;
        npfAudioArtist.before(artistDiv);
        npfAudioArtist.remove();
      }

      if(npfAudioAlbum){
        let albumText = npfAudioAlbum.textContent;
        if(albumText.trim() == ""){
          albumText = "Unknown Album";
        }
        let albumDiv = document.createElement("div");
        albumDiv.classList.add("aud-album");
        albumDiv.textContent = albumText;
        npfAudioAlbum.before(albumDiv);
        npfAudioAlbum.remove();
      }

      let aftermath = xyz.nextElementSibling;
      if(aftermath){
        if(aftermath.matches("img.album-cover")){
          xyz.before(aftermath); // put <img class='album-cover'> BEFORE .aud-xyz
          aftermath.classList.remove("album-cover");
        } else {
          // if .aud-xyz.next() !== img.album-cover,
          // make it
          let makeTheCover = document.createElement("img");
          makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
          xyz.before(makeTheCover);
        }
      }

      // if there is no .aud-xyz.next(),
      // make img.album-cover
      else {
        let makeTheCover = document.createElement("img");
        makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
        xyz.before(makeTheCover);
      }

      let audCover = document.createElement("div");
      audCover.classList.add("aud-cover");

      if(aftermath && aftermath.matches("img.album-cover")){
        aftermath.before(audCover);
        audCover.append(aftermath);
      } else {
        xyz.before(audCover);
        audCover.append(audCover.closest(".aud-gen").querySelector("img"));
      }

      let audCtl = document.createElement("div");
      audCtl.classList.add("aud-ctl");
      audCover.prepend(audCtl);

      // play container
      let cplay = document.createElement("div");
      cplay.classList.add("q-play");
      audCtl.append(cplay);

      // play icon
      let playIcon = document.createElement("span");
      playIcon.classList.add("play-icon");
      cplay.append(playIcon);

      // pause container
      let cpause = document.createElement("div");
      cpause.classList.add("q-pause");
      audCtl.append(cpause);

      // pause icon
      let pauseIcon = document.createElement("span");
      pauseIcon.classList.add("pause-icon");
      cpause.append(pauseIcon);

      // audio url
      let audSrc;
      let actualAud = npfAudioDiv.nextElementSibling; // was xyz.nextElementSibling
      if(actualAud){
        if(actualAud.matches("audio")){
          if(actualAud.matches("[src]")){
            audSrc = actualAud.getAttribute("src");
          } else if(actualAud.querySelector("source[src]")){
            audSrc = actualAud.querySelector("source[src]").getAttribute("src");
          }
        }
      }

      // make audio btn <a>
      let a = document.createElement("a");
      a.classList.add("aud-dl");
      a.href = audSrc;
      a.target = "_blank";
      a.ariaLabel = "Download";
      xyz.after(a);

      // make audio dl icon
      let ic = document.createElement("span");
      ic.classList.add("feather-icons");
      ic.setAttribute("icon-name","download");
      a.append(ic);

      // set the volume
      actualAud.volume = audVol;

      // play and pause events
      cplay.addEventListener("click", () => {
        if(actualAud.paused){
          actualAud.play();
        }
      });

      cpause.addEventListener("click", () => {
        if(!actualAud.paused){
          actualAud.pause();
        }
      });

      actualAud.addEventListener("play", () => {
        cplay.style.display = "none";
        cpause.style.display = "flex";
      });

      actualAud.addEventListener("pause", () => {
        cpause.style.display = "none";
        cplay.style.display = "flex";
      });

      actualAud.addEventListener("ended", () => {
        cpause.style.display = "none";
        cplay.style.display = "flex";

        // autoplay the next audio if there is one
        // npfAudioDiv == .aud-gen
        let figNext = npfAudioDiv.closest(".npf_audio").nextElementSibling; /* was npfAudioDiv.nextElementSibling */
        if(figNext && figNext.matches(".npf_audio")){
          figNext.querySelector(".q-play")?.click();
        }
      });
    }); //end npfAudio each

    // find out if npf audio needs to be relocated
    // aka if it's the first "thing" of the original caption/entry
    document.querySelectorAll(".original-caption > .aud-gen:first-child")?.forEach((npfAudio) => {
      // check if there's caption text AFTER the audio inst
      if(npfAudio.nextElementSibling){
        if(npfAudio.nextElementSibling.innerHTML.trim() == ""){
          // remove .original-entry and move .aud-gen outside .caption
          npfAudio.closest(".original-entry").classList.add("tbd");
          npfAudio.closest(".caption").before(npfAudio);
          npfAudio.nextElementSibling.querySelector(".original-entry.tbd")?.remove();
        }
      } else {
        // remove .original-entry and move .aud-gen outside .caption
        npfAudio.closest(".original-entry").classList.add("tbd");
        npfAudio.closest(".caption").before(npfAudio);
        npfAudio.closest(".caption").before(npfAudio);
      }
    });
    
    // honestly not sure why I commented this part out
    // i don't remember if this is needed
    // document.querySelectorAll(".original-caption > .aud-gen:first-child")?.forEach(npfAudio => {
    //     npfAudio.closest(".caption").before(npfAudio)
    // });
  }; //end neueAudio()
  

  /*------- EXTERNAL AUDIO -------*/
  let player_btn_color = getRoot("--Audio-Post-Buttons-BG");
  let soundcloud_height = Number(getRoot("--SoundCloud-Player-Height").replace(/[^\d\.]*/g,""));
  let scAlbumShowHide = getRoot("--SoundCloud-Show-Album-Image");
  soundcloud_height = !soundcloud_height ? 116 : soundcloud_height;
  scAlbumShowHide = scAlbumShowHide == "yes" ? "true" : "false"
  const soundcloudAudio = () => {
    // minimalist soundcloud player: @shythemes
    // shythemes.tumblr.com/post/114792480648
    document.querySelectorAll("iframe[src*='soundcloud.com']")?.forEach((sc) => {
      let curSrc = sc.getAttribute("src").split("&")[0];
      sc.src = `${curSrc}&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=${scAlbumShowHide}&amp;origin=tumblr&amp;color=${player_btn_color.split("#")[1]}`;
      (sc.height = soundcloud_height), (sc.width = "100%");
      
      setTimeout(() => {
        soundcloud_height == 20 ? sc.closest("figure")?.classList.add("sc-short") : null
      },0)
    });

  document.querySelectorAll(".caption .reblog-content.original-caption > [data-npf*='soundcloud.com']:first-child")?.forEach((sc) => {
      if(sc.nextElementSibling){
        if(sc.nextElementSibling.innerHTML.trim() == ""){
          // can remove original capt
          sc.closest(".original-entry").classList.add("tbd");
          sc.closest(".caption").before(sc);
          sc.nextElementSibling.querySelector(".tbd")?.remove();
        } else {
          // has next with stuff, don't relocate
        }
      } else {
        // can remove original capt
        sc.closest(".original-entry").classList.add("tbd");
        sc.closest(".caption").before(sc);
        sc.nextElementSibling.querySelector(".tbd")?.remove();
      }
    });
  };

  /*------- AUDIO EMBEDS -------*/
  const audioEmbeds = () => {
    document.querySelectorAll(`.caption > [data-npf*='"type":"audio","provider":']:first-child`)?.forEach((emb) => {
      if(emb.nextElementSibling){
        if(emb.nextElementSibling.innerHTML.trim() == ""){
          // move outside of capt
          emb.closest(".caption").before(emb);
          emb.nextElementSibling.remove();
        } else {
          // there's more caption text, do nothing
        }
      } else {
        // move outside of capt
        emb.closest(".caption").before(emb);
        emb.nextElementSibling.remove();
      }
    });

    document.querySelectorAll(`.original-entry .original-caption > [data-npf*='"type":"audio","provider":']:first-child`)?.forEach((emb) => {
      if(emb.nextElementSibling){
        if(emb.nextElementSibling.innerHTML.trim() == ""){
          // nothing after it, remove op
          emb.closest(".original-entry").classList.add("tbd");
          emb.closest(".caption").before(emb);
        } else {
          // there's more caption text, do nothing
        }
      } else {
        // nothing after it, remove op
        emb.closest(".original-entry").classList.add("tbd");
        emb.closest(".caption").before(emb);
      }

      let tbd = emb.nextElementSibling.querySelector(".original-entry.tbd");
      if(tbd){
        tbd.remove();
      }
    });
    
    
    document.querySelectorAll("[post-type='audio'] .post-body > iframe[class*='_audio_']:first-child")?.forEach(aud => {
      let r = document.createElement("figure");
      r.classList.add("tmblr-full");
      aud.before(r);
      r.append(aud)
      setTimeout(() => {
        if(typeof NPFv4 === "function"){
          NPFv4()
          setTimeout(() => {
            aud.closest(".npf_group").classList.add("npf_audio")
          },0)
        }
      },0)
    })
    
    // target .npf_inst.npf_audio and also add .npf_audio class to .npf_group (parent)
    setTimeout(() => {
      document.querySelectorAll(".post-body > .npf_group:not(.npf_audio):first-child > .npf_audio:first-child")?.forEach(a => {
        if(!a.querySelector(".aud-gen")){
          a.closest(".npf_group").classList.add("npf_audio")
        }
      })
    },0)
  };
  
  /*------- AUDIOS ARE NOT VIDEOS -------*/
  const audiosAreNotVideos = () => {
    document.querySelectorAll(".legacy-video")?.forEach((lv) => {
      let fi = lv.querySelector("iframe");
      if(fi && (fi.matches("iframe[src*='spotify.com']") || fi.matches("iframe[src*='soundcloud.com']"))){
        lv.before(fi);
        lv.remove();
      }
    });
  };
  
  /*------- LEGACY VIDEOS -------*/
  const legacyVideos = () => {
    document.querySelectorAll(".legacy-video .tumblr_video_container + .poster-thumb[url]:not([url=''])")?.forEach(poster => {
      let posterURL = poster.getAttribute("url");
      if(posterURL.trim() !== ""){
        let segment = posterURL.trim().split(".media.tumblr.com/tumblr_")[1].split("_")[0];
        
        let vidURL = `https://va.media.tumblr.com/tumblr_${segment}.mp4`
        fetch(vidURL).then(q => {
          if(!q.ok){
            // url does not exist
          }
          return q;
        }).then(c => {
          let newVid = document.createElement("video");
          newVid.src = vidURL;
          newVid.setAttribute("poster",posterURL);
          poster.after(newVid);
          
          if(typeof VIDYO === "function"){
            VIDYO("video")
          }
          
          poster.previousElementSibling.remove();
          poster.remove();
        }).catch(err => console.error(err))
      }
    })
  }

  /*------- VIDEO EMBEDS -------*/
  const videoEmbeds = () => {
    document.querySelectorAll(".caption > .tmblr-embed:first-child")?.forEach((emb) => {
      // move embed outside of caption
      emb.closest(".caption").before(emb);
    });

    document.querySelectorAll(".caption > .tmblr-embed:last-child")?.forEach((t) => {
      let capt = t.closest(".caption");
      if(capt.nextElementSibling){
        if(capt.nextElementSibling.matches(".infobar")){
          capt.style.marginBottom = "0px";
        }
      }
    });
  }; //end videoEmbeds()

  /*------- NPF VIDEOS -------*/
  let autoplayNPFVideos = getRoot("--Autoplay-NPF-Videos").toLowerCase();

  const npfVideos = () => {
    document.querySelectorAll(`.caption > .npf_row:first-child > [data-npf*='"type":"video"']`)?.forEach((v) => {
      v.classList.add("npf-photo-origin");
      v.closest(".caption").before(v);
    });

    document.querySelectorAll("video")?.forEach((v) => {
      if(autoplayNPFVideos == "true" || autoplayNPFVideos == "yes"){
        if(!v.matches("[autoplay]")){
          v.setAttribute("autoplay","");
        }
      } else {
        if(v.matches("[autoplay]")){
          v.removeAttribute("autoplay");
        }
      }
      
      v.muted = false;
    });
  };
  
  /*------- DEACTIVATED USERNAMES -------*/
  const deactivatedUsers = () => {
    document.querySelectorAll(".reblogs[username] a.reblog-head .reblog-user")?.forEach((de) => {
      let name = de.closest(".reblogs[username]").getAttribute("username").toString();
      
      if(name.indexOf("-") > -1 && name.substring(name.lastIndexOf("-")+1).slice(0,5) == "deact"){
        let cut_name = name.split("-deact")[0];
        de.textContent = cut_name;
        
        // append "(deactivated)" text display for the user
        let newc = document.createElement("span");
        newc.classList.add("deactivated");
        newc.textContent = "(deactivated)";
        de.after(newc);
        
        // add .deactivated class to the permalink in case there isn't one
        de.closest("a.reblog-head")?.classList.add("deactivated");
        de.closest("a.reblog-head.deactivated")?.removeAttribute("href");

        let img = de.closest(".reblog-head").querySelector(`img[alt*="${name}"]`);
        if(img){
          let img_alt = img.getAttribute("alt");
          img.alt = img_alt.replace(name, cut_name);
        }
      } 
    });
    
    document.querySelectorAll(".perma-info a[username]:not([username=''])")?.forEach((perma) => {
      let username = perma.getAttribute("username").toString().trim();
      if(username.indexOf("-") > -1 && username.substring(username.lastIndexOf("-")+1).slice(0,5) == "deact"){
        let noDeac = username.substring(0,username.lastIndexOf("-"));
        perma.textContent = perma.textContent.replaceAll(username,noDeac) + " (deactivated)";
        perma.matches("[href]") ? perma.removeAttribute("href") : null;
      }
    });
  };
  
  /*------- LOADING - REMOVE INVISIBLE PLACEHOLDERS -------*/
  const removeInvis = () => {
    document.querySelectorAll(".invis")?.forEach((invis) => {
      invis.classList.remove("invis");
    });
  };
  
  /*------- REMOVE EMPTY REBLOGS -------*/
  const removeEmptyReblogs = () => {
    let emptyList = [
      ".reblog-content > p:first-child",
      ".tmblr-full",
      ".npf_row",
      ".reblogs",
      ".caption, .photo-origin",
      ".chatline b.chat-label"
    ];
    emptyList.forEach((item) => {
      if(document.querySelector(item)){
        document.querySelectorAll(item).forEach((i) => {
          if(i.innerHTML.trim() === ""){
            i.remove();
          }
        });
      }
    });
  };

  /*------- PADDING AROUND ORIGINAL PHOTOS -------*/
  let pdyn = getRoot("--Padding-Around-Original-Photos").toLowerCase();
  // pdyn = 'no'
  const firstMediaPadding = () => {
    
    if(pdyn == "true" || pdyn == "yes"){
      document.documentElement.setAttribute("first-item-padding","yes");
      let tgs = `
        .post-body > .npf_group:first-child:not(.npf-link-block),
        .post-body > .tmblr-embed:first-child,
        .post-body > iframe:first-child,
        .post-body > .legacy-video:first-child,
        .post-body > .legacy-photo:first-child,
        .post-body > .legacy-photoset:first-child
      `
      setTimeout(() => {
        document.querySelectorAll(tgs)?.forEach((tg) => {
          const _addPadding = () => {
            let addPadding = document.createElement("div");
            addPadding.classList.add("add-padding");
            
            if(tg.nextElementSibling){
              if(!(tg.nextElementSibling.matches(".caption") || tg.nextElementSibling.matches(".tags"))){
                addPadding.classList.add("with-bottom");
              }
            } else {
              addPadding.classList.add("with-bottom");
            }
            
            tg.before(addPadding);
            addPadding.append(tg);
          }
          
          if(tg.querySelector(":scope > *:first-child")?.matches(".npf_audio") && tg.querySelector(`[data-npf*='"type":"audio","provider":']`)){
            _addPadding()
          } else if(!tg.querySelector("[post-type='audio']:not(.previously-npf) .aud-gen")){
            _addPadding()
          }
          
        });
      },0);
    } else {
      document.documentElement.setAttribute("first-item-padding","no");
    }
    
    
  };

  /*------- CHANGE CURLY QUOTES TO STRAIGHT ONES -------*/
  const uncurly = () => {
    document.querySelectorAll("pre, code, .npf_chat, .chat-content")?.forEach((code) => {
      let stuff = code.innerHTML;
      //stuff = stuff.replaceAll("“", '"').replaceAll("”", '"');
      stuff = stuff.replace(/\u201C/g,'"').replace(/\u201D/g,'"')
      code.innerHTML = stuff;
    });
  };

  /*------- CHAT POSTS -------*/
  const chatStuff = () => {
    document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
      let contents = chat.childNodes;
      if(contents){
        let textNodes = Array.from(contents).filter((node) => {
          return node.nodeType === 3 && node.data.trim().length > 0;
        });

        textNodes.forEach((node) => {
          let wrapper = document.createElement("span");
          node.parentNode.insertBefore(wrapper, node);
          wrapper.appendChild(node);
        });
      }

      /*------- CHAT LABELS -------*/
      // YES chat label
      if(chat.querySelector("b")){
        let checkB = chat.querySelectorAll(":scope > b");
        checkB?.forEach(b => {
          if(!b.matches(".chat-label")){
            b.classList.add("chat-label");
          }
        })
      }

      // NO chat label? force it
      // (make one & it will be empty)
      else {
        let makeB = document.createElement("b");
        makeB.classList.add("chat-label");
        chat.prepend(makeB)
      }

      /*------- CHAT CONTENT -------*/
      chat.querySelectorAll(":scope > b.chat-label")?.forEach((label, i) => {
        let next = label.nextElementSibling;

        // if it HAS a .next() element, put all of that into .chat-content
        if(next){
          let elle = "*:not(.chat-label)";
          label.parentNode.querySelectorAll(`:scope > ${elle}`)?.forEach(el => {
            let prevEl = elle.previousElementSibling;
            if(!prevEl?.matches(elle)){
              if(!el.closest(".chat-content")){
                let cont = document.createElement("span");
                cont.classList.add("chat-content");
                el.before(cont);

                let apres = cont.nextElementSibling;
                while(apres && apres.matches(elle)){
                  cont.appendChild(apres);
                  apres = cont.nextElementSibling;
                }
              }
            }
          })
        }      

        // no .next()
        // .chat-content is EMPTY
        else {
          let sp = document.createElement("span");
          sp.classList.add("chat-content");
          label.after(sp);
        }
      })

      /*------- WRAP EACH LINE -------*/
      chat.querySelectorAll(":scope > .chat-label + .chat-content")?.forEach(content => {
        let chatContent = content;
        let chatLabel = content.previousElementSibling;

        let line = document.createElement("li");
        line.classList.add("chatline");

        chatLabel.before(line);

        let next = line.nextElementSibling;
        while(next && (next.matches(".chat-label") || next.matches(".chat-content"))){
          line.append(next);
          next = line.nextElementSibling;
        }
      })

      /*------- REPLACE CHAT-WRAP -------*/


      setTimeout(() => {
        // make a new wrapper
        let newWrap = document.createElement("ul");
        newWrap.classList.add("chatwrap");
        chat.before(newWrap);

        chat.querySelectorAll(":scope > .chatline")?.forEach(line => {
          newWrap.append(line);
        })

        /*------- CLEAN UP THE CHAT LINES -------*/
        newWrap.querySelectorAll(".chatline")?.forEach(line => {
          let label = line.querySelector(".chat-label");
          let content = line.querySelector(".chat-content");

          // IF: LABEL TEXT BUT EMPTY CONTENT, USE ITS TEXT AS THE CHAT CONTENT
          if(label.innerHTML.trim() !== "" && content.innerHTML.trim() == ""){
            content.innerHTML = label.innerHTML;
            label.innerHTML = ""
          }

          // if label and chat BOTH have text in it,
          // but label does not end with :
          // but content starts with :
          let labelText = label.textContent.trim();
          let contentText = content.textContent.trim();
          if(contentText.slice(0,1) == ":"){
            if(labelText.slice(-1) !== ":"){
              label.append(":");
              content.textContent = content.textContent.slice(1)
            }
          }
        })//end line forEach

        /*------- DEAL W/ CASES OF .chatwrap + .chatwrap -------*/
        // example post: tumblr.com/glen-test/737075029801598976
        document.querySelectorAll(".chatwrap:first-of-type:not(:only-of-type)")?.forEach(firstChat => {
          let next = firstChat.nextElementSibling;
          while(next && next.matches(".chatwrap")){
            next.querySelectorAll(":scope > .chatline")?.forEach(line => {
              firstChat.append(line)
            })
            next.remove();
            next = firstChat.nextElementSibling;
          }
        })

        // remove the original chat
        chat.remove();
      },0);
    })//end chat forEach

    setTimeout(() => {
      // if chat is the first .comment, hide the comment header
      document.querySelectorAll("[post-type='chat'] .comment:first-child ul.chatwrap")?.forEach(c => {
        c.closest(".comment")?.classList.add("chat-first-comment")
      })
    },0)

  }//end chatStuff()
  
  /*------- NPF LINK BLOCKS -------*/
  const npfLinkBlocks = () => {
    document.querySelectorAll(".npf-link-block")?.forEach(link => {
      link.classList.add("npf_group")
      let title = link.querySelector(".title");
      let desc = link.querySelector(".description");
      if(title){
        let titleText = title.textContent;
        
        if(desc){
          desc.textContent = titleText
        } else {
          let bottom = link.querySelector(".bottom");
          if(bottom){
            let d = document.createElement("div");
            d.classList.add("description");
            d.textContent = titleText;
            bottom.prepend(d)
          }
        }
      }
      
      // trim .site=name
      let siteName = link.querySelector(".site-name");
      if(siteName){
        siteName.textContent = siteName.textContent.trim();
      }
    })
    
    setTimeout(() => {
      if(typeof NPFv4 === "function"){
        NPFv4()
      }
    },0)
  }
  
  /*------- ASK STUFF -------*/
  const askStuff = ()  => {
    document.querySelectorAll(".question-w")?.forEach(question => {
      let img = question.querySelector("img");
      let askerW = question.querySelector(".asker-w");
      let askerY = question.querySelector(".asker-y");
      if(img && askerW && askerY){
        setTimeout(() => {
          let imgH = img.offsetHeight;
          let askerHeadH = askerY.offsetHeight;
          if(askerHeadH <= imgH){
            askerW.classList.add("self-y-center");
          }
        },1)
      }
      
      // apply asker usernamename (plain text) to av's alt text
      let askerA = question.querySelector(".asker-name");
      if(askerA){
        let a = askerA.querySelector(":scope > a:first-child");
        // not-anonymous
        if(a){
          let t = a.textContent;
          askerA.setAttribute("username",t);
          if(img){
            img.alt = `${t}'s avatar`
          }
        }
        
        // anonymous
        else {
          let t = askerA.textContent.split(" ")[0];
          askerA.setAttribute("username",t);
          if(img){
            img.alt = `${t}'s avatar`
          }
        }
      }
    })
  }
  
  /*------- QUOTE STUFF -------*/
  const quoteStuff = () => {
    document.querySelectorAll("[post-type='text'] .caption > .reblogs:first-of-type .reblog-content > blockquote:first-child + p:last-child")?.forEach(peas => {
      let bbq = peas.previousElementSibling;
      let bbqFirstChild = bbq.querySelector(":scope > *:first-child");
      if(bbqFirstChild){
        if(bbqFirstChild.matches("div:only-child")){
          let bbqText = bbqFirstChild.innerHTML;
          bbq.prepend(bbqText);
          bbq.classList.add("npf_quote");
          bbq.classList.add("quote-text");
          bbqFirstChild.remove();
          
          if(peas.innerHTML.trim() !== ""){
            peas.classList.add("quote-source")
          }
          
          let reblogContent = peas.closest(".reblog-content");
          reblogContent?.classList.add("quote-container")
        }
      }
    })
  }
  
  /*------- "READ MORE", "KEEP READING" STUFF -------*/
  const keepReadingStuff = ()  => {
    document.querySelectorAll(".caption p:not([class]) > a")?.forEach(a => {
      if(a.matches(".read_more") || (a.textContent.trim() == "Keep reading")){
        
        // add .read_more class if it doesn't already have it
        if(!a.matches(".read_more")){
          a.classList.add("read_more")
        }
        
        // add .keep-reading to <p> (parent)
        a.parentNode.classList.add("keep-reading")
        
        let href = a.href;
        if(href.indexOf("/post/") > -1 && href.indexOf(".tumblr.com") > -1){
          if(href.slice(0,7) == "http://"){
            href = "https://" + href.slice(7)
          }
          let blogName = href.split("https://")[1].split(".tumblr.com")[0];
          let postID = href.split(blogName)[1].split("/post/")[1];
          if(postID.indexOf("/") > -1){
            postID = postID.split("/")[0]
          }
          
          // check if the "keep reading" blog post link is invalid
          let post = a.closest(".posts[username]:not([username=''])");
          if(post){
            let op = post.getAttribute("username").trim();
            if(op !== blogName){
              a.href = a.href.replaceAll(blogName, op);
              blogName = op;
            }
          }
          
          if(getRoot("--Keep-Reading-Open-Dashboard") == "yes"){
            a.href = `https://tumblr.com/${blogName}/${postID}`
          }
        }

        if(getRoot("--Keep-Reading-Open-In-New-Tab") == "yes"){
          a.target = "_blank"
        }
      }
    })
  }
  
  /*------- UNSTRETCH IFRAMES -------*/
  const unstretchIframes = () => {
    // try and prevent legacy videos from having stretched container height
    let tmblr_vid_iframes = document.querySelectorAll(".tumblr_video_container[style*='height:'] iframe");
    tmblr_vid_iframes?.forEach(tmblr_vid_iframes => {
        let iframe_parent = tmblr_vid_iframes.closest(".tumblr_video_container[style*='height:']");
        iframe_parent.style.height = ""
    })
  }
  
  /*------- REASSIGN POST TYPES -------*/
  const redoPostTypes = () => {
    document.querySelectorAll("[post-type='text'] .post-body")?.forEach(post => {
      // reassign post type as CHAT POST
      let findChat = post.querySelector(".caption > .reblogs:first-of-type .reblog-content > .chatwrap:first-child");
      if(findChat){
        post.closest("[post-type]").setAttribute("post-type","chat")
        post.closest("[post-type]").classList.add("previously-npf")
      }
      
      // reassign post type as QUOTE POST
      let findQuote = post.querySelector(".caption > .reblogs:first-of-type .reblog-content > .npf_quote:first-child + .quote-source:last-child");
      if(findQuote){
        post.closest("[post-type]").setAttribute("post-type","quote")
        post.closest("[post-type]").classList.add("previously-npf")
      }
      
      setTimeout(() => {
        let npfGroup = post.querySelector(":scope > .npf_group:first-child");
        if(npfGroup){
          let npf_inst = npfGroup.querySelector(".npf_inst");
          if(npf_inst){
            // reassign post type as PHOTO POST
            if(npf_inst.matches("[onclick]") || npf_inst.querySelector(".tmblr-full img")){
              post.closest("[post-type]").setAttribute("post-type","photo")
              post.closest("[post-type]").classList.add("previously-npf")
            }
            
            // reassign post type as VIDEO POST
            if(npf_inst.querySelector(".tmblr-embed") || npf_inst.matches(".npf_video")){
              post.closest("[post-type]").setAttribute("post-type","video")
              post.closest("[post-type]").classList.add("previously-npf")
            }

            // reassign post type as AUDIO POST
            if(npf_inst.matches(".npf_audio")){
              post.closest("[post-type]").setAttribute("post-type","audio")
              post.closest("[post-type]").classList.add("previously-npf")
            }
          }
          
          // reassign post type as LINK POST
          if(npfGroup.matches(".npf-link-block")){
            post.closest("[post-type]").setAttribute("post-type","link")
            post.closest("[post-type]").classList.add("previously-npf")
          }
        }
      },0)
      
      // reassign post type as POLL POST
      let findPoll = post.closest("[post-type]").querySelector(".poll-post:first-child");
      if(findPoll){
        post.closest("[post-type]").setAttribute("post-type","poll")
        post.closest("[post-type]").classList.add("previously-npf")
      }
      
      /*--------------*/
    })
  }
  
  const resonance = () => {
    // remove custard if it's not needed
    document.querySelectorAll("html[current-url]:not([current-url='/customize_preview_receiver.html']) body > .custard")?.forEach(custard => {
      custard.remove();
    })
    
    // prevent trackpad overscroll ONLY on desktops
    if(/Mobi|Android/i.test(navigator.userAgent)){
      // on mobile
      document.querySelectorAll("html, body")?.forEach(s => {
        s.matches(".no-overscroll") ? s.classList.remove("no-overscroll") : null        
      })
    }
    
    else {
      // on desktop
      document.querySelectorAll("html, body")?.forEach(s => {
        !s.matches(".no-overscroll") ? s.classList.add("no-overscroll") : null      
      })
    }
    
    
    // remove empty parts in right sidebar [1/2]
    document.querySelectorAll(".sidebar.right .section")?.forEach(s => {
      if(s.querySelector(":scope > *")){
        s.querySelectorAll(":scope > *")?.forEach(z => {
          if(z.innerHTML.trim() == ""){
            z.remove()
          }
        })
      }
    })
    
    // remove empty parts in right sidebar [2/2]
    document.querySelectorAll(".t-row, .sidebar.right .section")?.forEach(row => {
      if(row.innerHTML.trim() == ""){
        row.remove();
      }
    })
    
    // remove unused .posts.brief
    document.querySelectorAll(".posts.brief:not([id^='post-'],[post-type])")?.forEach(z => {
      let e = getComputedStyle(z).getPropertyValue("display");
      let i = z.style.display;
      if(e == "none" || i == "none"){
        z.remove();
      }
    })
    
    // music player
    let jirousplayer = document.querySelector(".jirous-player");
    let j_ctrls = document.querySelector(".jirous-player .ctrls");
    let j_play = document.querySelector(".jirous-player .ctrls .play");
    let j_pause = document.querySelector(".jirous-player .ctrls .pause");
    let j_audio = document.querySelector(".jirous-player audio[src]:not([src=''])");
    if(jirousplayer && j_ctrls && j_play && j_pause && j_audio){
      let audURL = j_audio.getAttribute("src");
      
      // fix dropbox links
      audURL = audURL.replace("//www.dropbox.com","//dl.dropbox.com").replaceAll("&dl=0","");
      
      
      // check if URL is valid
      fetch(audURL).then(q => {
        if(!q.ok){
          throw new Error(`Couldn't access audio url: ${audURL}`);
        }
        return q;
      }).then(c => {
        j_play.addEventListener("click", () => {
          if(j_audio.paused){
            j_audio.play();
          }
        })

        j_pause.addEventListener("click", () => {
          if(!j_audio.paused){
            j_audio.pause();
          }
        })

        j_audio.addEventListener("play", () => {
          j_play.style.display = "none";
          j_pause.style.display = "block";
        })

        j_audio.addEventListener("pause", () => {
          j_play.style.display = "block";
          j_pause.style.display = "none";
        })

        if(!j_audio.matches("[loop]")){
          j_audio.addEventListener("ended", () => {
            j_play.style.display = "block";
            j_pause.style.display = "none";
          })
        }        
          
      }).catch(err => console.error(err));
      
      // volume
      if(j_audio.matches("[volume]:not([volume=''])")){
        let vol = j_audio.getAttribute("volume").replace(/[^\d\.]*/g,"");
        vol = Number(vol);
        if(!isNaN(vol)){
          // if there's a "%" in the string
          if(j_audio.getAttribute("volume").trim().indexOf("%") > -1){
            vol = vol/100;
          }
          
          j_audio.volume = vol;
        }
      } 
      
    }//end: if audio components exist
  }//end reso
  
  const tippys = (el) => {
    document.querySelectorAll(`[${el}]:not([${el}=''])`)?.forEach(x => {
      if(x.getAttribute(el).trim() !== ""){
        tippy(x, {
          content: x.getAttribute(el),
          maxWidth: "var(--Post-Width)",
          followCursor: true,
          arrow: false,
          offset: [0, 18],
          moveTransition: "transform 0.069s ease-out",
        })
      }
    })
  }

  /*------- HELLO? HORSE IS CALLING -------*/
  
  // get rid of href.li
  noHrefLi();
  
  // check reblog-header's op "name" to see if it's actually the op
  theRealOP();
  
  // photoset stuff
  handleLegacyPhotos();

  // audio stuff
  legacyAudio();
  neueAudio();
  soundcloudAudio();
  audiosAreNotVideos();
  audioEmbeds();

  // video stuff
  legacyVideos();
  videoEmbeds();
  npfVideos();  
  if(typeof VIDYO === "function"){
    VIDYO("video")
  }
  
  // chat stuff
  chatStuff();
  
  // ask posts, answer posts
  askStuff();
  
  // quote posts
  quoteStuff();
  
  // "read more", "keep reading"
  keepReadingStuff();
  
  // other stuff
  deactivatedUsers();
  
  uncurly();
  removeEmptyReblogs();
  npfLinkBlocks();
  npfMisc();
  unstretchIframes();
  
  setTimeout(() => {
    redoPostTypes();
    firstMediaPadding();
  },0)
  
  // resonance stuff
  resonance();
  tippys("aria-label");
  removeInvis();
}); //end DOMContentLoaded